
#ifndef SRHOOKPE_H
#define SRHOOKPE_H

#include "SRBaseHook.h"
#include "fn2void.hpp"
#include <SRSignal/SRSignal.hpp>
//#	include <mutex>
//#	include <atomic>
#ifdef WIN32
#	include <windows.h>
#	include <dbghelp.h>
#else
#	include <asm/cachectl.h>
#endif
#include "cshortasm.h"
#include "memsafe.h"

namespace SRHook::PE {
	template<call_t ct, typename T> class Hook;
	template<call_t ct, typename R, typename... Args> class Hook<ct, R( Args... )> {
		std::string lib, func;
		/*std::atomic_*/ bool enabled;
		/*std::atomic_*/ bool calling;
		bool installed;
		R result;
		bool resset;
		CShortAsm *rasm;
		size_t ECX;
		void *original;
		//		std::mutex		 mtx;

	public:
		Hook( std::string_view lib, std::string_view func ) :
			lib( lib ), func( func ), enabled( false ), resset( false ), calling( false ) {
			//			std::lock_guard lock( mtx );
			rasm = new CShortAsm();
		}
		~Hook() {
			//			std::lock_guard lock( mtx );
			if ( installed ) {
				auto _lib = GetModuleHandleA( lib.data() );
				if ( _lib && _lib != INVALID_HANDLE_VALUE ) {
					auto _func = (void *)GetProcAddress( _lib, func.data() );
					if ( _func && _func == original ) {
						if ( SetProcAddress( _lib, func.data(), original ) ) {
							enabled = false;
							installed = false;
						}
					}
				}
			}
			if ( enabled ) disable_unsafe();
			if ( !installed ) delete rasm;
		}

		bool enable() {
			//			std::lock_guard lock( mtx );
			if ( enabled ) return false;
			if ( !installed ) {
				auto _lib = GetModuleHandleA( lib.data() );
				if ( !_lib || _lib == INVALID_HANDLE_VALUE ) return false;
				original = (void *)GetProcAddress( _lib, func.data() );
				if ( !original ) return false;
				if ( SetProcAddress( _lib, func.data(), (void *)rasm->getAddr() ) ) {
					enable_unsafe();
					installed = true;
				}
			} else
				enable_unsafe();
			return enabled;
		}

		bool disable() {
			//			std::lock_guard lock( mtx );
			if ( !enabled ) return false;
			disable_unsafe();
			return true;
		}

		bool hasEnabled() { return enabled; }

		bool setReturn( const R &value ) {
			if ( !calling ) return false;
			result = std::move( value );
			resset = true;
			return true;
		}

		bool callOriginal( Args... args ) {
			if ( !calling ) return false;
			if constexpr ( ct == call_t::thiscall )
				orig_thiscall( args... );
			else
				orig_call( args... );
			resset = true;
			return true;
		}

		SRSignal<Args &...> onHook;
		size_t retAddr;

	protected:
		R hook( Args &...args ) {
			retAddr = *(size_t *)retAddr;
			//			std::lock_guard lock( mtx );
			resset = false;
			calling = true;
			if constexpr ( ct == call_t::thiscall )
				thiscall( args... );
			else
				call( args... );
			calling = false;
			return result;
		}

		void call( Args &...args ) {
			onHook( args... );
			if ( !resset ) orig_call( args... );
		}

		template<typename This, typename... ThisArgs> void thiscall( This, ThisArgs &...args ) {
			onHook( (This &)ECX, args... );
			if ( !resset ) orig_thiscall( ECX, args... );
		}

		void orig_call( Args &...args ) {
			typedef R( __stdcall * __scall )( Args... );
			typedef R( __cdecl * __ccall )( Args... );
			if constexpr ( ct == call_t::ccall )
				result = ( __ccall( original ) )( args... );
			else
				result = ( __scall( original ) )( args... );
		}

		template<typename This, typename... ThisArgs> void orig_thiscall( This, ThisArgs &...args ) {
			typedef R( __thiscall * __call )( size_t self, ThisArgs... );
			result = ( __call( original ) )( ECX, args... );
		}

		void enable_unsafe() {
			using hook_t = std::remove_reference_t<decltype( *this )>;
			rasm->resetWriteOffset();
			uint8_t saveESP[6] = { 0x89, 0x25, 0x44, 0x33, 0x22, 0x11 };
			*reinterpret_cast<size_t *>( saveESP + 2 ) = reinterpret_cast<size_t>( &retAddr );
			rasm->insert( saveESP, 6 );
			uint8_t thissafe[6] = { 0x89, 0x0d, 0x44, 0x33, 0x22, 0x11 };
			*reinterpret_cast<size_t *>( thissafe + 2 ) = reinterpret_cast<size_t>( &ECX );
			rasm->insert( thissafe, 6 );
			rasm->mov( r86::ECX, reinterpret_cast<const int>( this ) );
			for ( int i = sizeof...( Args ) - 1, so = 4; i >= 0; --i, so += 4 ) {
				rasm->mov( r86::EAX, r86::ESP );
				rasm->add( r86::EAX, static_cast<const int>( so + i * 4 ) );
				rasm->push( r86::EAX );
			}
			rasm->call( reinterpret_cast<int>( fn2void( &hook_t::hook ) ) );
			thissafe[0] = 0x8b;
			rasm->insert( thissafe, 6 );
			if constexpr ( ct == call_t::ccall )
				rasm->ret();
			else
				rasm->ret( ( sizeof...( Args ) ) * 4 );
#ifdef WIN32
			FlushInstructionCache( GetCurrentProcess(), (void *)rasm->getAddr(), rasm->getSize() );
#else
			cacheflush( (void *)rasm->getAddr(), rasm->getSize(), ICACHE );
#endif
			enabled = true;
		}

		void disable_unsafe() {
			rasm->resetWriteOffset();
			rasm->jmp( reinterpret_cast<int>( original ) );
#ifdef WIN32
			FlushInstructionCache( GetCurrentProcess(), (void *)rasm->getAddr(), rasm->getSize() );
#else
			cacheflush( (void *)rasm->getAddr(), rasm->getSize(), ICACHE );
#endif
			enabled = false;
		}

		static bool SetProcAddress( PVOID ModuleBase, PCHAR pFunctionName, PVOID pFunctionAddress ) {
			// Based on KernelGetProcAddress from ReactOS
			ULONG size = 0;
			ImageDirectoryEntryToData( ModuleBase, TRUE, IMAGE_DIRECTORY_ENTRY_EXPORT, &size );
			PIMAGE_DOS_HEADER dos = (PIMAGE_DOS_HEADER)ModuleBase;
			PIMAGE_NT_HEADERS nt = (PIMAGE_NT_HEADERS)( (ULONG)ModuleBase + dos->e_lfanew );
			PIMAGE_DATA_DIRECTORY expdir = (PIMAGE_DATA_DIRECTORY)( nt->OptionalHeader.DataDirectory + IMAGE_DIRECTORY_ENTRY_EXPORT );
			ULONG addr = expdir->VirtualAddress;
			PIMAGE_EXPORT_DIRECTORY exports = (PIMAGE_EXPORT_DIRECTORY)( (ULONG)ModuleBase + addr );
			PULONG functions = (PULONG)( (ULONG)ModuleBase + exports->AddressOfFunctions );
			PSHORT ordinals = (PSHORT)( (ULONG)ModuleBase + exports->AddressOfNameOrdinals );
			PULONG names = (PULONG)( (ULONG)ModuleBase + exports->AddressOfNames );
			ULONG max_name = exports->NumberOfNames;
			ULONG max_func = exports->NumberOfFunctions;

			for ( ULONG i = 0; i < max_name; i++ ) {
				ULONG ord = ordinals[i];
				if ( i >= max_name || ord >= max_func ) return false;
				if ( functions[ord] < addr || functions[ord] >= addr + size ) {
					if ( strcmp( (PCHAR)ModuleBase + names[i], pFunctionName ) == 0 ) {
						memsafe::Unprotect( (DWORD)functions + ord, 4 );
						functions[ord] = (PCHAR)pFunctionAddress - (PCHAR)ModuleBase;
#ifdef WIN32
						FlushInstructionCache( GetCurrentProcess(), (void *)( functions + ord ), 4 );
#else
						cacheflush( (void *)( functions + ord ), 4, ICACHE );
#endif
						return true;
					}
				}
			}
			return false;
		}
	};

	template<call_t ct, typename R, typename... Args>
	void make_for( Hook<ct, R( Args... )> *&hook, std::string_view lib, std::string_view func ) {
		hook = new Hook<ct, R( Args... )>( lib, func );
	}
} // namespace SRHook::PE

#endif // SRHOOKPE_H
