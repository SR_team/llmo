#include "PatchBase.h"
#include "memsafe.h"
#include <stdexcept>

using namespace std::string_literals;

SRHook::PatchBase::PatchBase( void *address, int size, std::string_view module )
	: address( address ), size( size ), module( module ) {}

SRHook::PatchBase::PatchBase(std::size_t address, int size, std::string_view module )
	: PatchBase( (void *)address, size, module ) {}

SRHook::PatchBase::~PatchBase() {
	PatchBase::disable();
	if ( original ) delete[] original;
}

void SRHook::PatchBase::changeAddr( void *address ) {
	this->address = address;
}

void SRHook::PatchBase::changeAddr(std::size_t address)
{
	changeAddr((void *)address);
}

void SRHook::PatchBase::changeSize( int size ) {
	this->size = size;
}

void SRHook::PatchBase::changeModule( std::string_view module ) {
	this->module = module;
}

void SRHook::PatchBase::enable() {
	if ( !original ) {
		original = new unsigned char[size];
		memsafe::copy( original, getAddress(), size );
	}
}

void SRHook::PatchBase::disable() {
	if ( original ) memsafe::copy( getAddress(), original, size );
}

void *SRHook::PatchBase::getAddress() {
	if ( module.empty() )
		return address;
	else{
#ifdef _WIN32
		return (void *)( (std::size_t)address + (std::size_t)GetModuleHandleA( module.c_str() ) );
#else
		throw std::runtime_error(__PRETTY_FUNCTION__ + ": temporary modules supported only on WIN32"s);
#endif
	}
}
