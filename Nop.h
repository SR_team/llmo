#ifndef SRHOOK_NOP_H
#define SRHOOK_NOP_H

#include "PatchBase.h"

namespace SRHook {
	class Nop : public PatchBase {
	public:
		Nop( void *address, int size = 5, std::string_view module = std::string_view() ) : PatchBase( address, size, module ) {}
		Nop( std::size_t address, int size = 5, std::string_view module = std::string_view() ) : PatchBase( address, size, module ) {}

		virtual void enable();
	};

} // namespace SRHook

#endif // SRHOOK_NOP_H
