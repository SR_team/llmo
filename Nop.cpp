#include "Nop.h"
#include "memsafe.h"

void SRHook::Nop::enable() {
	PatchBase::enable();
	memsafe::set( getAddress(), 0x90, size );
}
