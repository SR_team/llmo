#pragma once

#include <cassert>
#include <type_traits>

#if __has_include( <MinHook.h>)
#	include <MinHook.h>
#elif __has_include( "MinHook.h" )
#	include "MinHook.h"
#endif

#include "SRAllocator.h"
#include "fn2void.hpp"

namespace SRHook {
	namespace details {
		template<class This, typename Ret, typename... Args> ptr_t method_trampoline( Ret ( This::*method )( Args... ), This *obj ) {
			static constexpr size_t size = 15;

			using OrigClass = typename std::tuple_element<0, std::tuple<Args...>>::type;
			static_assert( std::is_pointer<OrigClass>::value, "method_trampoline: First argument in Args must be a pointer" );
			static_assert( std::is_void<std::remove_pointer_t<OrigClass>>::value || std::is_class<std::remove_pointer_t<OrigClass>>::value,
						   "method_trampoline: First argument in Args must be a class or void*. Example: `void CPed::SetHitpoints(CPed *, "
						   "float)` or `void CPed::SetHitpoints(void *, float)`" );


			auto trampoline = Allocator().allocate( size );
			if ( trampoline.ptr == nullptr ) return trampoline;
			auto *code_ptr = trampoline.ptr;

			// pop EAX (remove ret addr from stack)
			*code_ptr = 0x58;
			code_ptr++;

			// push ECX (pass default CTX as first arg)
			*code_ptr = 0x51;
			code_ptr++;

			// push EAX (restore ret addr to stack)
			*code_ptr = 0x50;
			code_ptr++;

			// mov ECX, obj (set custom CTX)
			*code_ptr = 0xB9;
			code_ptr++;
			*reinterpret_cast<void **>( code_ptr ) = obj;
			code_ptr += sizeof( void * );

			// mov EAX, method
			*code_ptr = 0xB8;
			code_ptr++;
			*reinterpret_cast<void **>( code_ptr ) = fn2void( method );
			code_ptr += sizeof( void * );

			// jmp EAX
			*code_ptr = 0xFF;
			code_ptr++;
			*code_ptr = 0xE0;
			code_ptr++;

			trampoline.flush();
			assert( code_ptr - trampoline.ptr == size );
			return trampoline;
		}
	} // namespace details


	/**
	 * @brief Creates method trampolines for hooking member functions
	 * 
	 * @tparam Ret Return type of the target function
	 * @tparam Args Argument types of the target function
	 * 
	 * @example Example hook for CPed::SetAmmo method:
	 * \code{.cpp}
	 * class ExampleHook {
     *     using trampoline_t = SRHook::Trampoline<void(CPed*, int, int)>;
	 *     trampoline_t trampoline;
	 *     trampoline_t::method_type SetAmmo;
	 *     std::map<int, int> extraAmmo; // Weapon ID -> extra ammo
	 *
	 *     void Hook_SetAmmo(CPed* ped, int weapon, int ammo) {
	 *         // Add extra ammo before original call
	 *         if(extraAmmo.contains(weapon)) {
	 *             ammo += extraAmmo[weapon];
	 *         }
	 *
	 *         // Call original function through trampoline
	 *         (ped->*SetAmmo)(weapon, ammo);
	 *     }
	 * 
	 * public:
	 *     ExampleHook() {
	 *         // Create trampoline for CPed::SetAmmo
	 *         trampoline.install(&ExampleHook::Hook_SetAmmo, this);
	 *
	 *         // Initialize minhook
	 *         MH_Initialize();
	 *
	 *         // Create hook at address 0x005DF290 using trampoline entrypoint
	 *         MH_CreateHook(
	 *             reinterpret_cast<void*>(0x005DF290),
	 *             const_cast<void *>(trampoline.entrypoint()),
	 *             reinterpret_cast<void**>(&SetAmmo)
	 *         );
	 *
	 *         MH_EnableHook(reinterpret_cast<void*>(0x005DF290));
	 *     }
	 * 
	 *     ~ExampleHook() {
	 *         MH_DisableHook(reinterpret_cast<void*>(0x005DF290));
	 *         MH_RemoveHook(reinterpret_cast<void*>(0x005DF290));
	 *
	 *         MH_Uninitialize();
	 *     }
	 * } g_plugin;
	 * \endcode
	 */
	template<typename T> class Trampoline;
	template<typename Ret, class This, typename... Args> class Trampoline<Ret( This, Args... )> {
		ptr_t trampoline;

	public:
		using method_type = Ret ( std::remove_pointer_t<This>::* )( Args... );

		Trampoline() = default;
		template<class Object> Trampoline( Ret ( Object::*method )( This, Args... ), Object *obj ) {
			trampoline = details::method_trampoline( method, obj );
		}
		~Trampoline() {
			if ( trampoline.ptr != nullptr ) Allocator().deallocate( trampoline );
			trampoline = {};
		}

		template<class Object> const void *install( Ret ( Object::*method )( This, Args... ), Object *obj ) {
			if ( trampoline.ptr != nullptr ) Allocator().deallocate( trampoline );
			trampoline = details::method_trampoline( method, obj );
			return trampoline.ptr;
		}

		[[nodiscard]] const void *entrypoint() const { return trampoline.ptr; }
	};

	/**
	 * @brief Creates method trampolines for hooking member functions
     * @details Fat version contains pointer to the original method
	 * 
	 * @tparam Ret Return type of the target function
	 * @tparam Args Argument types of the target function
	 * 
	 * @example Example hook for CPed::SetAmmo method:
	 * \code{.cpp}
	 * class ExampleHook {
     *     SRHook::FatTrampoline<void( CPed *, int, int )> SetAmmo; // trampoline + original method pointer
	 *     std::map<int, int> extraAmmo; // Weapon ID -> extra ammo
	 *
	 *     void Hook_SetAmmo(CPed* ped, int weapon, int ammo) {
	 *         // Add extra ammo before original call
	 *         if(extraAmmo.contains(weapon)) {
	 *             ammo += extraAmmo[weapon];
	 *         }
	 *
	 *         // Call original function through trampoline
	 *         (ped->*SetAmmo)(weapon, ammo); // GCC10 bug 80654 workaround: (ped->**SetAmmo)(weapon, ammo);
	 *     }
	 * 
	 * public:
	 *     ExampleHook() {
	 *         // Create trampoline for CPed::SetAmmo
	 *         trampoline.install(&ExampleHook::Hook_SetAmmo, this);
	 *
	 *         // Initialize minhook
	 *         MH_Initialize();
	 *
	 *         // Create hook at address 0x005DF290 using trampoline entrypoint
	 *         MH_CreateHook(
	 *             reinterpret_cast<void*>(0x005DF290),
	 *             const_cast<void *>(trampoline.entrypoint()),
	 *             SetAmmo.original()
	 *         );
	 *
	 *         MH_EnableHook(reinterpret_cast<void*>(0x005DF290));
	 *     }
	 * 
	 *     ~ExampleHook() {
	 *         MH_DisableHook(reinterpret_cast<void*>(0x005DF290));
	 *         MH_RemoveHook(reinterpret_cast<void*>(0x005DF290));
	 *
	 *         MH_Uninitialize();
	 *     }
	 * } g_plugin;
	 * \endcode
	 */
	template<typename T> class FatTrampoline;
	template<typename Ret, typename... Args> class FatTrampoline<Ret( Args... )> : public Trampoline<Ret( Args... )> {
		Trampoline<Ret( Args... )>::method_type method;

	public:
		using Trampoline<Ret( Args... )>::Trampoline;
		using Trampoline<Ret( Args... )>::install;
		using Trampoline<Ret( Args... )>::entrypoint;

		operator typename Trampoline<Ret( Args... )>::method_type() const { return method; }
		Trampoline<Ret( Args... )>::method_type operator*() const { return method; } // For GCC <11 compatibility (see bugs 80654 and 85209)

		/// Pointer to the original method
		void **original() { return reinterpret_cast<void **>( &method ); }
	};

#if __has_include( <MinHook.h>) || __has_include( "MinHook.h")
	/**
	 * @brief Creates method trampolines for hooking member functions
     * @details Fat version contains pointer to the original method
	 * 
	 * @tparam Ret Return type of the target function
	 * @tparam Args Argument types of the target function
	 * 
	 * @example Example hook for CPed::SetAmmo method:
	 * \code{.cpp}
	 * class ExampleHook {
     *     SRHook::TrampolineHook<void( CPed *, int, int )> SetAmmo{0x005DF290};
	 *     std::map<int, int> extraAmmo; // Weapon ID -> extra ammo
	 *
	 *     void Hook_SetAmmo(CPed* ped, int weapon, int ammo) {
	 *         // Add extra ammo before original call
	 *         if(extraAmmo.contains(weapon)) {
	 *             ammo += extraAmmo[weapon];
	 *         }
	 *
	 *         // Call original function through trampoline
	 *         (ped->*SetAmmo)(weapon, ammo); // GCC10 bug 80654 workaround: (ped->**SetAmmo)(weapon, ammo);
	 *     }
	 * 
	 * public:
	 *     ExampleHook() {
	 *         // Initialize minhook
	 *         MH_Initialize();
     *
	 *         // Create trampoline for CPed::SetAmmo
	 *         trampoline.install_and_enable(&ExampleHook::Hook_SetAmmo, this);
	 *     }
	 * 
	 *     ~ExampleHook() {
     *         // This two methods can be called automatically, but we handle MH initialization in this class
	 *         SetAmmo.disable();
	 *         SetAmmo.remove();
	 *
	 *         MH_Uninitialize();
	 *     }
	 * } g_plugin;
	 * \endcode
	 */
	template<typename T> class TrampolineHook;
	template<typename Ret, typename... Args> class TrampolineHook<Ret( Args... )> {
		FatTrampoline<Ret( Args... )> trampoline;
		void *address;

	public:
		TrampolineHook( void *address ) : address( address ) {}
		TrampolineHook( size_t address ) : address( reinterpret_cast<void *>( address ) ) {}
		~TrampolineHook() {
			if ( disable() == MH_OK ) remove();
		}

		operator typename FatTrampoline<Ret( Args... )>::method_type() const { return trampoline; }
		FatTrampoline<Ret( Args... )>::method_type operator*() const { // For GCC <11 compatibility (see bugs 80654 and 85209)
			return trampoline;
		}

		template<class Object> MH_STATUS install( Ret ( Object::*method )( Args... ), Object *obj ) {
			if ( trampoline.install( method, obj ) == nullptr ) return MH_ERROR_MEMORY_ALLOC;

			return MH_CreateHook( address, const_cast<void *>( trampoline.entrypoint() ), trampoline.original() );
		}

		template<class Object> MH_STATUS install_and_enable( Ret ( Object::*method )( Args... ), Object *obj ) {
			const auto status = install( method, obj );
			if ( status == MH_OK ) return enable();
			return status;
		}

		MH_STATUS remove() { return MH_RemoveHook( address ); }

		MH_STATUS enable() { return MH_EnableHook( address ); }
		MH_STATUS disable() { return MH_DisableHook( address ); }

		MH_STATUS queue_enable() { return MH_QueueEnableHook( address ); }
		MH_STATUS queue_disable() { return MH_QueueDisableHook( address ); }
	};
#endif
} // namespace SRHook