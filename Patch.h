#ifndef SRHOOK_PATCH_H
#define SRHOOK_PATCH_H

#include "PatchBase.h"
#include <vector>
#if __has_include( <array>)
#	include <array>
#endif

namespace SRHook {

	class Patch : public PatchBase {
		std::vector<unsigned char> data;

	public:
		Patch( void *address, const std::vector<unsigned char> &data, std::string_view module = std::string_view() ) :
			PatchBase( address, data.size(), module ), data( data ) {}
#if __has_include( <array>)
		template<std::size_t len>
		Patch( void *address, const std::array<unsigned char, len> &data, std::string_view module = std::string_view() ) :
			PatchBase( address, data.size(), module ), data( data.begin(), data.end() ) {}
#endif
		Patch( std::size_t address, const std::vector<unsigned char> &data, std::string_view module = std::string_view() ) :
			PatchBase( address, data.size(), module ), data( data ) {}
#if __has_include( <array>)
		template<std::size_t len>
		Patch( std::size_t address, const std::array<unsigned char, len> &data, std::string_view module = std::string_view() ) :
			PatchBase( address, data.size(), module ), data( data.begin(), data.end() ) {}
#endif

		virtual void enable();
	};

} // namespace SRHook

#endif // SRHOOK_PATCH_H
