#ifndef REDIRECT_HPP
#define REDIRECT_HPP

#include "cshortasm.h"
#include "fn2void.hpp"
#include "memsafe.h"
#include <mutex>
#include <string>
#include <windows.h>

namespace SRHook {
	template<typename T> class Redirect;
	template<typename R, typename... Args> class Redirect<R( Args... )> {
		std::string module;
		size_t		address;
		size_t		original;
		CShortAsm * rasm   = nullptr;
		bool		hooked = false;
		std::mutex	mtx;

	public:
		size_t ECX;

		Redirect( size_t address, std::string_view module = std::string_view() )
			: address( address ), module( module ) {}
		virtual ~Redirect() {
			std::lock_guard lock( mtx );
			remove();
			if ( !hooked ) delete rasm;
		}

		virtual bool changeHook( size_t address, std::string_view module = std::string_view() ) {
			std::lock_guard lock( mtx );
			if ( hooked ) return false;
			this->address = address;
			this->module  = module;
			return true;
		}

		virtual bool changeAddr( size_t address ) {
			std::lock_guard lock( mtx );
			if ( hooked ) return false;
			this->address = address;
			return true;
		}

		virtual bool changeModule( std::string_view module = std::string_view() ) {
			std::lock_guard lock( mtx );
			if ( hooked ) return false;
			this->module = module;
			return true;
		}

		template<class C> bool install( C *obj, R ( C::*method )( Args... ) ) {
			std::lock_guard lock( mtx );
			if ( hooked ) return false;
			auto originalAddr = writeAddr();
			if ( !originalAddr ) return false;
			rasm->resetWriteOffset();
			uint8_t thissafe[6]							= { 0x89, 0x0d, 0x44, 0x33, 0x22, 0x11 };
			*reinterpret_cast<size_t *>( thissafe + 2 ) = reinterpret_cast<size_t>( &ECX );
			rasm->insert( thissafe, 6 );
			rasm->mov( r86::ECX, reinterpret_cast<const int>( obj ) );
			rasm->jmp( reinterpret_cast<int>( fn2void( method ) ) );
			if ( *(uint8_t *)originalAddr != 0xE9 && *(uint8_t *)originalAddr != 0xE8 )
				memsafe::write<uint8_t>( (void *)originalAddr, 0xE9 );
			memsafe::write<ssize_t>( (void *)( originalAddr + 1 ),
									 ssize_t( rasm->getAddr() ) - ( ssize_t( originalAddr ) + 5 ) );
			hooked = true;
			return true;
		}

		bool install( R ( *method )( Args... ) ) { return pure_install( fn2void( method ) ); }

		bool pure_install( void *method ) {
			std::lock_guard lock( mtx );
			if ( hooked ) return false;
			auto originalAddr = writeAddr();
			if ( !originalAddr ) return false;
			rasm->resetWriteOffset();
			uint8_t thissafe[6]							= { 0x89, 0x0d, 0x44, 0x33, 0x22, 0x11 };
			*reinterpret_cast<size_t *>( thissafe + 2 ) = reinterpret_cast<size_t>( &ECX );
			rasm->insert( thissafe, 6 );
			rasm->jmp( reinterpret_cast<int>( method ) );
			if ( *(uint8_t *)originalAddr != 0xE9 && *(uint8_t *)originalAddr != 0xE8 )
				memsafe::write<uint8_t>( (void *)originalAddr, 0xE9 );
			memsafe::write<ssize_t>( (void *)( originalAddr + 1 ),
									 ssize_t( rasm->getAddr() ) - ( ssize_t( originalAddr ) + 5 ) );
			hooked = true;
			return true;
		}

		bool remove() {
			std::lock_guard lock( mtx );
			if ( !hooked ) return false;
			auto originalAddr = writeAddr();
			if ( !originalAddr ) return false;
			memsafe::write<ssize_t>( (void *)originalAddr, original );
			hooked = false;
			return true;
		}

	protected:
		size_t writeAddr() {
			if ( !rasm ) rasm = new CShortAsm();
			auto originalAddr = address;
			if ( !module.empty() ) {
#ifdef WIN32
				auto mod = GetModuleHandleA( module.c_str() );
				if ( mod == nullptr || mod == INVALID_HANDLE_VALUE ) return 0;
				originalAddr += (size_t)mod;
#else
				return false;
#endif
			}
			original = memsafe::read<size_t>( (void *)( originalAddr + 1 ) );
			return originalAddr;
		}
	};
} // namespace SRHook

#endif // REDIRECT_HPP
