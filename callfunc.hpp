#ifndef CALLFUNC_H
#define CALLFUNC_H

#include <cstdint>
#include <cstddef>

namespace CallFunc {
	template<typename T = void *, class... Args> T thiscall( const void *object, const void *method, Args... args ) {
#ifdef _WIN32
		typedef T( __thiscall * __call )( const void *_this, Args... );
#else
		typedef T( * __call )( const void *_this, Args... ) __attribute__((thiscall));
#endif
		return ( __call( method ) )( object, args... );
	}
	template<typename T = void *, class... Args> T thiscall( const void *object, const std::size_t method, Args... args ) {
		return thiscall<T>( object, (const void *)method, args... );
	}
	template<typename T = void *, class... Args> T thiscall( const std::size_t object, const void *method, Args... args ) {
		return thiscall<T>( (const void *)object, method, args... );
	}
	template<typename T = void *, class... Args> T thiscall( const std::size_t object, const std::size_t method, Args... args ) {
		return thiscall<T>( (const void *)object, (const void *)method, args... );
	}

	template<typename T = void *, class... Args> T stdcall( const void *method, Args... args ) {
#ifdef _WIN32
		typedef T( __stdcall * __call )( Args... );
#else
		typedef T( * __call )( Args... ) __attribute__((stdcall));
#endif
		return ( __call( method ) )( args... );
	}
	template<typename T = void *, class... Args> T stdcall( const std::size_t method, Args... args ) {
		return stdcall<T>( (const void *)method, args... );
	}

	template<typename T = void *, class... Args> T ccall( const void *method, Args... args ) {
#ifdef _WIN32
		typedef T( __cdecl * __call )( Args... );
#else
		typedef T( * __call )( Args... ) __attribute__((cdecl));
#endif
		return ( __call( method ) )( args... );
	}
	template<typename T = void *, class... Args> T ccall( const std::size_t method, Args... args ) {
		return ccall<T>( (const void *)method, args... );
	}
} // namespace CallFunc

#endif // CALLFUNC_H
