#ifndef CSHORTASM_H
#define CSHORTASM_H

#include <cstdint>
#include <map>
#include <string>
#include <vector>

#define CALLHOOK void __stdcall

typedef enum r86
{
	EAX = 0,
	ECX,
	EDX,
	EBX,
	ESP,
	EBP,
	ESI,
	EDI
} r86;

template<typename T> union byteValue {
	T value;
	uint8_t bytes[sizeof( T )];
};

class CShortAsm {
public:
	CShortAsm( std::size_t pages = 1 );
	~CShortAsm();

	void insert( uint8_t *array, std::size_t size );
	void insert( std::vector<uint8_t> array );
	void push( uint8_t value );
	void push( ssize_t value );
	void push( r86 r );
	void pushad();
	void pushfd();
	void pop( r86 r );
	void popad();
	void popfd();
	void label( std::string label );
	void jmp( ssize_t addr );
	void jmp( std::string label );
	void jmp( r86 r );
	void ret( ssize_t pops = 0 );
	void nop();
	void call( ssize_t addr );
	void call( std::string label );
	void call( r86 r );
	void mov( ssize_t &var, r86 r ); // mov var, EAX
	void mov( r86 r, ssize_t &var ); // mov EAX, var
	void mov( r86 r, const ssize_t value ); // mov EAX, value
	void mov( r86 r1, r86 r2 ); // mov EAX, ECX
	void mov( r86 r1, r86 r2, uint8_t offset ); // mov EAX, [EAX + 4]
	void mov( r86 r1, uint8_t offset, r86 r2 ); // mov [EAX + 4], EAX
	void xchg( r86 r1, r86 r2, uint8_t offset = 0 ); // swap registers
	void xchg( r86 r, ssize_t &var );
	void add( r86 r, uint8_t value );
	void add( r86 r, const ssize_t value );
	void add( r86 r, ssize_t &var );
	void sub( r86 r, uint8_t value );
	void sub( r86 r, const ssize_t value );
	void sub( r86 r, ssize_t &var );
	void mul( r86 r );
	void imul( r86 r );
	void imul( r86 r1, r86 r2 );
	void imul( r86 r, uint8_t value );
	void imul( r86 r, const ssize_t value );
	void imul( r86 r1, r86 r2, uint8_t value );
	void imul( r86 r1, r86 r2, const ssize_t value );
	void div( r86 r );
	void idiv( r86 r );
	void cmp( r86 r1, r86 r2 );
	void cmp( r86 r, uint8_t value );
	void cmp( r86 r, const ssize_t value );
	void XOR( r86 r1, r86 r2 );
	void XOR( r86 r, uint8_t value );
	void XOR( r86 r, const ssize_t value );
	void je( const ssize_t addr );
	void je( std::string label );
	void jne( const ssize_t addr );
	void jne( std::string label );
	void jl( const ssize_t addr );
	void jl( std::string label );
	void jle( const ssize_t addr );
	void jle( std::string label );
	void jg( const ssize_t addr );
	void jg( std::string label );
	void jge( const ssize_t addr );
	void jge( std::string label );
	void jb( const ssize_t addr );
	void jb( std::string label );
	void jbe( const ssize_t addr );
	void jbe( std::string label );
	void ja( const ssize_t addr );
	void ja( std::string label );
	void jae( const ssize_t addr );
	void jae( std::string label );

	void resetWriteOffset();
	std::size_t getWriteOffset();
	void setWriteOffset( std::size_t offset );

	const uint8_t *getAddr();
	std::size_t getSize();

	static std::size_t arrayToPages( std::size_t size_array );

protected:
	void resize( std::size_t pages );
	std::size_t getRelativeAddress( std::size_t addr );
	void write( uint8_t v );
	void *myalloc( std::size_t size );
	void myfree( void *ptr, std::size_t size );

	static std::size_t pageSize();

	uint8_t *_code = nullptr;
	std::size_t _size = 0;

private:
	std::size_t _offset = 0;
	std::size_t _peak = 0;
	std::map<std::string, std::size_t> _labels;
	std::map<std::string, std::size_t> _jmps;
	std::map<std::string, std::size_t> _jes;
	std::map<std::string, std::size_t> _jnes;
	std::map<std::string, std::size_t> _jls;
	std::map<std::string, std::size_t> _jles;
	std::map<std::string, std::size_t> _jgs;
	std::map<std::string, std::size_t> _jges;
	std::map<std::string, std::size_t> _jbs;
	std::map<std::string, std::size_t> _jbes;
	std::map<std::string, std::size_t> _jas;
	std::map<std::string, std::size_t> _jaes;
};

#endif // CSHORTASM_H
