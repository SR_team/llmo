#include "Patch.h"
#include "memsafe.h"

void SRHook::Patch::enable() {
	PatchBase::enable();
	memsafe::copy( getAddress(), data );
}
