#ifndef SRHOOK_PATCHBASE_H
#define SRHOOK_PATCHBASE_H

#include <string>
#include <string_view>

namespace SRHook {

	class PatchBase {
	protected:
		unsigned char *original = nullptr;
		void *		   address	= nullptr;
		int			   size		= 5;
		std::string	   module;

	public:
		PatchBase( void *address, int size = 5, std::string_view module = std::string_view() );
		PatchBase( std::size_t address, int size = 5, std::string_view module = std::string_view() );
		virtual ~PatchBase();

		virtual void changeAddr( void *address );
		virtual void changeAddr(std::size_t address );
		virtual void changeSize( int size );
		virtual void changeModule( std::string_view module );

		virtual void enable();
		virtual void disable();

		void *getAddress();
	};

} // namespace SRHook

#endif // SRHOOK_PATCHBASE_H
