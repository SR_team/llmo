#include "SRHookFast.h"
#include <cstdarg>

void SRHook::Fast::Hook::dbg( const char *fmt, ... ) {
	if ( debugOverride ) {
		if ( fmt == nullptr ) return;
		static char buf[4096]{ 0 };
		va_list argptr;
		va_start( argptr, fmt );
		vsprintf( buf, fmt, argptr );
		va_end( argptr );
		if ( module.empty() )
			dbg_out( true, "{0x%X, %d}: %s", addr, size, buf );
		else
			dbg_out( true, "{0x%X, %d, %s}: %s", addr, size, module.c_str(), buf );
	}
}
SRHook::Fast::Hook::~Hook() {
	while ( stage != CallingStage::wait )
		;
	SRHook::Fast::Hook::remove();
	if ( originalCode ) {
		delete[] originalCode;
		originalCode = nullptr;
	}
	if ( !hooked && code_ptr.ptr ) Allocator().deallocate( code_ptr );
	dbg( nullptr );
}

bool SRHook::Fast::Hook::changeHook( std::size_t addr, ssize_t size, std::string_view module ) {
	if ( hooked ) return false;
	if ( originalCode ) {
		delete[] originalCode;
		originalCode = nullptr;
	}
	if ( code_ptr.ptr ) Allocator().deallocate( code_ptr );
	this->addr = addr;
	this->size = size;
	this->module = module;
	return true;
}

bool SRHook::Fast::Hook::install() {
	if ( stage != CallingStage::wait ) return false;
	stage = CallingStage::install;
	codeLength = 0;

	if ( !remove_EAX ) remove_EAX = new std::size_t;
	if ( !remove_raddr ) remove_raddr = new std::size_t;

	// Копирование оригинального кода в класс
	if ( !originalCode ) {
		originalAddr = addr;
		if ( !module.empty() ) {
#ifdef _WIN32
			auto mod = GetModuleHandleA( module.data() );
			if ( mod == nullptr || mod == INVALID_HANDLE_VALUE ) {
#else
			auto mod = dlopen( module.data(), RTLD_NOW ); // NOTE: Это не указатель на начало либы!!!
			if ( mod == nullptr ) {
#endif
				Allocator().deallocate( code_ptr );
				stage = CallingStage::wait;
				return false;
			}
			originalAddr += (std::size_t)mod;
		}
		if ( !size ) {
			Allocator().deallocate( code_ptr );
			stage = CallingStage::wait;
			return false;
		}
		originalCode = new std::uint8_t[size];
		memsafe::copy( originalCode, (void *)originalAddr, size );
	}

	// Копирование EAX в класс
	pusha<std::uint8_t>( 0xA3, remove_EAX );
	// Перемещение адреса возврата в класс (обязательно до копирования ESP)
	pusha<std::uint8_t, std::uint8_t>( 0x58, 0xA3, remove_raddr );
	// Восстановление EAX из класса
	pusha<std::uint8_t>( 0xA1, remove_EAX );

	// Сохранение регистров
	push<std::uint8_t>( 0x60 );
	// Вызов обработчика перед оригинальным кодом
	pusha<std::uint8_t>( 0xB9, (std::size_t)this ); // mov ecx, this
	auto relAddr = getRelAddr( (std::size_t)code_ptr.ptr + codeLength, (std::size_t)fn2void( &Hook::before ) );
	pusha<std::uint8_t>( 0xE8, relAddr ); // call before

	// Восстановление регистров
	push<std::uint8_t>( 0x61 );

	// оригинальный код
	if ( !pushOriginal() ) {
		stage = CallingStage::wait;
		return false;
	}

	// Сохранение регистров
	push<std::uint8_t>( 0x60 );

	// Вызов обработчика после оригинального кода
	pusha<std::uint8_t>( 0xB9, (std::size_t)this ); // mov ecx, this
	relAddr = getRelAddr( (std::size_t)code_ptr.ptr + codeLength, (std::size_t)fn2void( &Hook::after ) );
	pusha<std::uint8_t>( 0xE8, relAddr ); // call after

	// Восстановление регистров
	push<std::uint8_t>( 0x61 );

	// Копирование EAX в класс
	pusha<std::uint8_t>( 0xA3, remove_EAX );
	// Восстановление адреса возврата из класса
	pusha<std::uint8_t>( 0xA1, remove_raddr ); // mov eax, retAddr
	push<std::uint8_t>( 0x50 ); // push eax
	// Восстановление EAX из класса
	pusha<std::uint8_t>( 0xA1, remove_EAX );

	// Выход из хука
	push<std::uint8_t>( 0xC3 ); // ret

	// Вход в хук
	if ( !hooked ) {
		auto relAddr = getRelAddr( originalAddr, (std::size_t)code_ptr.ptr );
		memsafe::write<std::uint8_t>( (void *)originalAddr, 0xE8 );
		memsafe::write<std::size_t>( (void *)( originalAddr + 1 ), relAddr );
		if ( size > 5 ) memsafe::set( (void *)( originalAddr + 5 ), 0x90, size - 5 );
		hooked = true;
	}

	stage = CallingStage::wait;
	dbg( "install to %08X", originalAddr );
	return true;
}

bool SRHook::Fast::Hook::remove() {
	for ( int i = 0; stage == CallingStage::before || stage == CallingStage::after; ++i ) {
		std::this_thread::yield();
		if ( i > 1000 ) return false;
		;
	}
	if ( stage != CallingStage::wait ) return false;
	if ( !hooked ) return false;
	if ( !originalCode ) return false;
	if ( !code_ptr.ptr ) return false;
	stage = CallingStage::remove;

	if ( memsafe::read<std::uint8_t>( (void *)originalAddr ) == 0xE8 ) {
		auto rel = memsafe::read<std::size_t>( (void *)( originalAddr + 1 ) );
		auto dest = getDestAddr( originalAddr, rel );
		if ( dest == (ssize_t)code_ptr.ptr ) {
			memsafe::copy( (void *)originalAddr, originalCode, size );
			hooked = false;
			stage = CallingStage::wait;
			dbg( "remove from %08X", originalAddr );
			return true;
		}
	}

	codeLength = 0;

	// Копирование EAX в класс
	pusha<std::uint8_t>( 0xA3, remove_EAX );
	// Перемещение адреса возврата в класс (обязательно до копирования ESP)
	pusha<std::uint8_t, std::uint8_t>( 0x58, 0xA3, remove_raddr );
	// Восстановление EAX из класса
	pusha<std::uint8_t>( 0xA1, remove_EAX );

	// оригинальный код
	if ( !pushOriginal() ) {
		stage = CallingStage::wait;
		return false;
	}

	// Копирование EAX в класс
	pusha<std::uint8_t>( 0xA3, remove_EAX );
	// Восстановление адреса возврата из класса
	pusha<std::uint8_t>( 0xA1, remove_raddr ); // mov eax, retAddr
	push<std::uint8_t>( 0x50 ); // push eax
	// Восстановление EAX из класса
	pusha<std::uint8_t>( 0xA1, remove_EAX );

	push<std::uint8_t>( 0xC3 ); // ret

	stage = CallingStage::wait;
	dbg( "remove from %08X (compability layer)", originalAddr );
	return true;
}

void SRHook::Fast::Hook::before() {
	stage = CallingStage::before;
	dbg( "call before" );
	onBefore();
#ifdef _GLIBCXX_STRING
	[[maybe_unused]] std::function<void()> gcc10_cruch = [] {
	};
#endif
}

void SRHook::Fast::Hook::after() {
	stage = CallingStage::after;
	dbg( "call after" );
	onAfter();
#ifdef _GLIBCXX_STRING
	[[maybe_unused]] std::function<void()> gcc10_cruch = [] {
	};
#endif
}
