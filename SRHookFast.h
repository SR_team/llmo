#ifndef SRHOOKFAST_HPP
#define SRHOOKFAST_HPP

// C++17
//#include <atomic>
#include <string>
#include <string_view>
#include <thread>
#include <type_traits>
#include <vector>
// This project
#include "SRAllocator.h"
#include "SRBaseHook.h"
#include "fn2void.hpp"
#include "memsafe.h" // winonly!

#include <SRSignal/SRSignal.hpp>

#ifdef _MSC_VER
#	ifdef _WIN64
typedef __int64 ssize_t;
#	else
typedef int ssize_t;
#	endif
#endif

#if !defined( _WIN32 )
#	include <dlfcn.h>
#endif

namespace SRHook::Fast {
	class Hook {
		enum class CallingStage
		{
			wait = 0,
			before,
			after,
			install,
			remove
		};

		std::size_t addr;
		ssize_t size;
		std::string module;

		bool debugOverride;
		void dbg( const char *fmt, ... );

	public:
		Hook( std::size_t addr, ssize_t size, std::string_view module = "", bool debugOverride = debug ) :
			addr( addr ), size( size ), module( module ), debugOverride( debugOverride ) {}
		virtual ~Hook();

		virtual bool isHooked() { return hooked; }

		virtual bool changeHook( std::size_t addr, ssize_t size, std::string_view module = "" );
		virtual bool changeHook( std::size_t addr, std::string_view module = "" ) { return changeHook( addr, -1, module ); }

		virtual bool changeAddr( std::size_t addr ) { return changeHook( addr, size, module ); }
		virtual bool changeSize( std::size_t size ) { return changeHook( addr, size, module ); }
		virtual bool changeModule( std::string_view module ) { return changeHook( addr, size, module ); }

		virtual std::size_t getAddr() const { return addr; }
		virtual std::size_t getSize() const { return size; }
		virtual const std::string_view getModule() const { return module; }

		SRSignal<> onBefore;
		SRSignal<> onAfter;

		virtual bool install();

		virtual bool remove();

		static ssize_t getRelAddr( ssize_t from, ssize_t to, std::size_t opLen = 5 ) { return to - ( from + opLen ); }
		static ssize_t getDestAddr( ssize_t from, ssize_t relAddr, std::size_t opLen = 5 ) { return relAddr + ( from + opLen ); }

	protected:
		/*std::atomic<*/ CallingStage /*>*/ stage = CallingStage::wait; // on x86 int is atomic

		std::size_t *remove_EAX = nullptr;
		std::size_t *remove_raddr = nullptr;

		std::size_t originalAddr;
		std::uint8_t *originalCode = nullptr;

		std::size_t codeLength = 0;
		ptr_t code_ptr{ nullptr, 0 };

		bool hooked = false;

		void before();
		void after();

	private:
		void push( std::uint8_t *data, std::size_t length ) {
			if ( length + codeLength >= code_ptr.size ) alloc();
			for ( std::size_t i = 0; i < length; ++i ) code_ptr.ptr[codeLength++] = data[i];
		}

		template<typename T> void push( const T &value ) {
			if ( sizeof( T ) + codeLength >= code_ptr.size ) alloc();
			if constexpr ( sizeof( T ) > 1 ) {
				union _ {
					_( T value ) : value( value ) {}
					T value;
					std::uint8_t bytes[sizeof( T )];
				} dec( value );
				for ( int i = 0; i < sizeof( T ); ++i ) code_ptr.ptr[codeLength++] = dec.bytes[i];
			} else
				code_ptr.ptr[codeLength++] = (std::uint8_t)value;
		}

		template<typename T, typename... Ts> void pusha( T value, Ts... values ) {
			push( value );
			if constexpr ( sizeof...( Ts ) > 1 )
				pusha( values... );
			else
				push( values... );
		}

		bool pushOriginal() {
			// Вставка оригинального кода
			push( originalCode, size );
			// Модификация оригинального кода
			auto firstOpcode = originalCode[0]; // NOTE: Только первый опкод, потому что в последним может
			// быть не опкод, а операнд одного из опкодов
			if ( firstOpcode == 0xE8 || firstOpcode == 0xE9 || firstOpcode == 0x0F ) {
				if ( firstOpcode == 0x0F ) {
					firstOpcode = originalCode[1];
					if ( firstOpcode >= 0x81 && firstOpcode <= 0x8F ) {
						auto dest = getDestAddr( originalAddr, *(std::size_t *)&originalCode[2], 6 );
						auto rel = getRelAddr( (std::size_t)&code_ptr.ptr[codeLength - size], dest, 6 );
						*(std::size_t *)&code_ptr.ptr[codeLength - ( size - 2 )] = rel;
					}
				} else {
					auto dest = getDestAddr( originalAddr, *(std::size_t *)&originalCode[1] );
					auto rel = getRelAddr( (std::size_t)&code_ptr.ptr[codeLength - size], dest );
					*(std::size_t *)&code_ptr.ptr[codeLength - ( size - 1 )] = rel;
				}
			}
			return true;
		}

		void alloc() {
			if ( !code_ptr.ptr ) {
				code_ptr = Allocator().allocate( 58 + size );
				if ( !code_ptr.ptr ) dbg( "Can't allocate memory for code" );
			} else {
				if ( !Allocator().reallocate( code_ptr, code_ptr.size * 2 ) ) dbg( "Can't reallocate memory for code" );
			}
		}
	};
} // namespace SRHook::Fast

#endif // SRHOOKFAST_HPP
